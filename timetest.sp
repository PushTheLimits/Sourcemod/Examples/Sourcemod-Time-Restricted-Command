//# vim: set filetype=cpp 
#pragma semicolon 1
#pragma newdecls required

public Plugin myinfo = 
{
	name = "Time Test",
	author = "RAYs3T",
	description = "Makes a command only available after a defined time ...",
	version = "0.1",
	url = "http://ptl-clan.de"
}

// Use a page like this (https://www.unixtimestamp.com/) to calculate your required timestamps.
int g_allowFromTime = 0;

public void OnPluginStart() {

	RegConsoleCmd("set_allow_from", Cmd_SetAllowedFrom);
	RegConsoleCmd("timed_command", Cmd_TimedCommand);
}

// Set time starting time.
// off course, this should not be a public command... Maybe put it into a config ...
public Action Cmd_SetAllowedFrom(int client, int args){
	char allowedTimeParam[32];
	GetCmdArg(1, allowedTimeParam, 32);

	g_allowFromTime = StringToInt(allowedTimeParam);
	PrintToChat(client, "Updated allowed time");
	
}

public Action Cmd_TimedCommand(int client, int args){
	// When executing the command, check if you have reched the expected time.
	// Maybe add also a second time for creating a "range" (from ... to) within executing the command is allowed	
	int currentTime = GetTime();
	if(currentTime < g_allowFromTime){
		// Executing this command is not permitted
		// You may could use FormatTime here to output the valid range to the user 
		PrintToChat(client, "Sorry, no time for santa yet");
		return Plugin_Continue;
	}
	
	// Yaaay, we can call santa!
	PrintToChatAll("HURAAY!!! EXTRA SPECIAL 1337 MONSTER EVENT HAPPENING NOW!");
	return Plugin_Continue;
}